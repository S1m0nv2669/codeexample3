﻿using System.Collections.Generic;
using ThineGames.Data;
using ThineGames.UI;
using UnityEngine;

/// <summary>
/// Логика конструирования
/// </summary>
public class ConstructorLogic : BaseDisposable
{
    public int ActualTemplateSlotNumber //номер слота игрок редактирует, нужен в том числе для того, чтобы сделать скриншот танка
    {
        get
        {
            return _shipSlotPm.CurrentPreviewPoint.slotId;
        }
    }

    public int SelectedMountPointNumber { get; private set; } //номер текущего выбранного конструкторского слота под пушки
    public List<TableElem> TemplateList = new List<TableElem>(); //отдельный список элементов, только для нижнего списка в готовыми шаблонами

    private List<ConstructorSlotView>  _allChildList = null; //список ссылок на все элементы, куда можно что-то установить
    private ConstructorCenterPresenter _constructorCenterPresenter;
    private ConstructorSlotPresenter   _constructorSlotPresenter;

    private ShipSlotPm _shipSlotPm;
    private ReactiveTrigger<int> _onClickSelect;
    private ReactiveTrigger<int> _onClickMount;

    private readonly GameDesignConfig _gameDesignConfig;

    private InventoryItem _currentTemplateSlot //слот, который в данный момент редактирует игрок
    {
        get
        {
            var tmp = ShipSlotManager.Instance.PodiumSlots[ActualTemplateSlotNumber].TemplateItem;

            if (tmp != null)
            {
                //Debug.Log("Slot != null");
                return tmp;
            }
            else
            {
                Debug.Log("Slot is Empty");
                return null;
            }
        }
    }

    public ConstructorLogic(ConstructorCenterView constructorController,
                            ConstructorSlotPresenter constructorSlotPresenter,
                            ShipSlotPm shipSlotPm,
                            ShipScreener shipScreener,
                            ReactiveTrigger<int> onClickMount,
                            ReactiveTrigger<int> onClickSelect,
                            GameDesignConfig gameDesignConfig
                            /*, ParentConstructorSlotView parentEqupmentSlotView*/)
    {
        _gameDesignConfig = gameDesignConfig;
        _onClickMount = onClickMount;
        _onClickSelect = onClickSelect;

        _shipSlotPm = shipSlotPm;

        _allChildList = new List<ConstructorSlotView>();
        _constructorCenterPresenter = new ConstructorCenterPresenter();

        _constructorCenterPresenter.SetCtx(constructorController, shipSlotPm);
        //_constructorCenterPresenter.SetParentEqupmentSlotView(parentEqupmentSlotView);
        _constructorSlotPresenter = constructorSlotPresenter;

        //подписываемся на делегат окончания делания скриншота танка, после смены любой шмотки на нём
        shipScreener.eventScreenTook += UpdateTemplateScreenImage;

        AddUnsafe(_onClickSelect.Subscribe(OnSelectElement));
        AddUnsafe(_onClickMount.Subscribe(OnMountElement));
    }

    //вызывается по клику на слот
    public void SetSelectedMountPointNumber(int number)
    {
        SelectedMountPointNumber = number;
        _constructorSlotPresenter.SetSelect(number);
    }

    //очищает темплейт и перезагружает его из сейва, чтобы откатить изменения
    public void ReInitModelInCurrentSlot()
    {
        _constructorSlotPresenter.ClearAllSlotsViews();
        InventoryManager.Instance.ReloadTemplateSlot(ActualTemplateSlotNumber);
        _shipSlotPm.CurrentPreviewPoint.ReInitModelInCurrentSlot();
    }

    public void ClearCurrentRealII()
    {
        //_constructorSlotPresenter.ClearAllSlotsViews();
        _shipSlotPm.CurrentPreviewPoint.ClearCurrentRealII();
    }

    //сбрасывать выбранный слот в нулевой нужно при каждом пересоздании слотов, чем бы оно не было вызвано
    private void SelectDefaulConstructorSlot()
    {
        SetSelectedMountPointNumber(0);
    }


    //установить предмет в шаблон без сохранения.
    //Все предметы экипировки включая корпус ставятся в шаблон через этот метод
    //исключение - загрузка шаблона целиком при старте игры
    private void SetItemToTemplate(ShopItem shopItem)
    {
        if (_currentTemplateSlot == null) //по идее такого быть не может, потому что слот всегда выбран
        {
            Debug.LogError("ConstructorController.Instance.CurCreatingShip == null");
            return;
        }

        if (InventoryManager.Instance.GetItemIdFromSlot(_currentTemplateSlot, SelectedMountPointNumber) == shopItem.Item.id)
        {
            Debug.Log("<color=blue>Item with name: " + shopItem.Item.id + " is already mounted in slot number: " + SelectedMountPointNumber + "</color>");  //предмет, который мы ставим и так лежит в этом слоте
            return;
        }

        Debug.Log("SetItemToTemplate = " + shopItem.Item.name);

        if (shopItem.Item.itemType == ItemTypeEnum.Corpus)
        {
            AddCorpusToTemplate(_currentTemplateSlot, shopItem.Item, _shipSlotPm.CurrentEq);
        }
        else
        {
            AddEquipmentToTemplate(_currentTemplateSlot, shopItem.Item, _shipSlotPm.CurrentEq, SelectedMountPointNumber);
        }
    }

    private MountController AddCorpusToTemplate(InventoryItem currentTemplateSlot, InventoryItem itemToAdd, EquipmentController equipmentController)
    {
        _constructorSlotPresenter.ClearAllSlotsViews(); //удаляем иконки слотов
        int equipmentSlotNumber = -1;

        MountController mountController = AddItemToTemplate(currentTemplateSlot, itemToAdd, equipmentController, equipmentSlotNumber);

        //если мы заменили корпус - нужно удалить пушки, который не влезли из темплейта
        //и переспавнить ГО для тех пушек, которые влезли
        RemoveExcessWeaponsForNewCorpus(currentTemplateSlot, mountController, equipmentSlotNumber);
        ReInitWeaponForNewCorpus(ActualTemplateSlotNumber);
        //CreateAllSlotsForCorpus(mountController);

        return mountController;
    }

    private MountController AddEquipmentToTemplate(InventoryItem currentTemplateSlot, InventoryItem itemToAdd,  EquipmentController equipmentController,
                                         int equipmentSlotNumber)
    {
        MountController mountController = AddItemToTemplate(currentTemplateSlot, itemToAdd, equipmentController, equipmentSlotNumber);
        UpdateSlotViewWithNewItem(itemToAdd, equipmentSlotNumber);

        //Добавляем камуфляж
        if (itemToAdd.itemType == ItemTypeEnum.Camo)
        {
            /* //любой объект типа InventoryItem должен быть создан через Instantiate, если его нужно потом применять как 
                //Монобехавор и он был не == null
                ItemLink = Instantiate(itemToAdd.item);

                //-- Применяем камуфляж ---------
                //Debug.Log("ConstructorController.Instance.RealII = " + ConstructorController.Instance.RealII);
                GameObject Hull = ConstructorController.Instance.RealII.GetUsedItem(ItemTypeEnum.Platform).item.gameObject;
                var shipCamoApplier = Hull.GetComponent<ShipCamoApplier>();

                if (shipCamoApplier != null)
                {
                    shipCamoApplier.Camouflage = itemToAdd.item as CamoInventoryItem;
                }
                //------------------------------

                //todo тут отображение визуала камуфляжа
                //ConstructorController.Instance.SetAbilityVisual(item.item, this);
                */
        }

        return mountController;
    }

    private MountController AddItemToTemplate(InventoryItem currentTemplateSlot, InventoryItem itemToAdd,
                                                EquipmentController equipmentController, int equipmentSlotNumber)
    {
        //удаляем старый объект в этом же слоте
        RemoveItemFromTamplateBySlotNumber(currentTemplateSlot, equipmentController, equipmentSlotNumber);
        _constructorCenterPresenter.RemoveItemFromTamplateVisualise(equipmentSlotNumber, ActualTemplateSlotNumber);

        Debug.Log("AddItemToTemplate item = " + itemToAdd.id);

        //Добавление в Json
        InventoryManager.Instance.AddItemToTemplate(_currentTemplateSlot,
                                            itemToAdd,
                                            count: 1,
                                            maxCount: 1,
                                            stack: false,
                                            save: false,
                                            slotNumber: equipmentSlotNumber,
                                            isUsed: true);

        //Визуализация
        MountController mountController = _constructorCenterPresenter.AddItemToTemplateVisualise(
                                            _shipSlotPm.CurrentPreviewPoint.CurrentTankModel,
                                            itemToAdd,
                                            _shipSlotPm.CurrentPreviewPoint.transform,
                                            equipmentSlotNumber,
                                            ActualTemplateSlotNumber);

        return mountController;
    }

    public void CreateAllSlotsForCorpus(MountController mountController)
    {
        //создаём UI слота под пушку
        if (mountController != null)
        {
            _constructorSlotPresenter.CreateAllSlotsForCorpus(mountController.GetMountPoints());
        }

        SelectDefaulConstructorSlot();
    }

    //повернули платформу и должны пересоздать UI под слоты для данной платформы, если они есть
    public void CreateSlotsForCurrentCorpus()
    {
        var realII = _shipSlotPm.CurrentPreviewPoint.RealII;
        _constructorSlotPresenter.CreateSlotsForCurrentCorpus(realII);
        SelectDefaulConstructorSlot();
    }

    //инициализируем все треугольнички выбранных пушек и слотов танков для текущего танка на платформе
    public void InitAllColorintTriangleForCurrentSlot()
    {
        var realII = _shipSlotPm.CurrentPreviewPoint.RealII;
        HangarController.Instance.GetGunTechCenterLogic().InitAllColorintTriangle(realII);
    }

    //обновить параметры предмета в слоте
    public void UpdateSlotViewWithNewItem(InventoryItem weaponItem, int slotNumber)
    {
        _constructorSlotPresenter.UpdateSlotViewWithNewItem(weaponItem, slotNumber);
    }

    public void UpdateCorpusCharacteristics(InventoryItem corpusItem)
    {
        _constructorSlotPresenter.UpdateCorpusCharacteristics(corpusItem);
    }

    /// <summary>
    /// Убрать из ячейки корпус.
    /// Удаляет из слота элемент, возвращает его в инвентарь игроку
    /// </summary>
    /*public void RemovePlatformFromSlot()
    {
        if (CorpusElem.inventoryItem != null)
        {
            ClearAllSlotList(true);//очистить всё с возвратом
        }
    }*/

    public void AddToAllChildList(ConstructorSlotView elem)
    {
        _allChildList.Add(elem);
    }

    /// <summary>
    /// Удаление элемента нужно потому, что когда мы удаляем основу танка,
    /// то её удаление приводит к удалению очерних слотов и их нужно убрать из общего списка
    /// </summary>
    /// <param name="elem"></param>
    public void RemoveFromChildList(ConstructorSlotView elem)
    {
        _allChildList.Remove(elem);
    }

    /// <summary>
    /// Очищает все слоты, включая базовый, все дочерние - удаляет
    /// </summary>
    /// <param name="WithReturn">Если true - вещи будут вынуты из шаблона в возвращены в инвентарь</param>
   /* public void ClearAllSlotList(bool WithReturn)
    {
        //Debug.Log("ClearAllChildList");

        foreach (var elem in _allChildList)
        {
            if (WithReturn && elem.inventoryItem != null) //в слоте осталась шмотка, перед удалением объекта слота нужно вернуть её назад в инвентарь игроку
            {
                InventoryManager.Instance.AddPlayerItem(elem.inventoryItem, 1); //возвращаем шмотку
                InventoryManager.Instance.RemoveItemFromShip(CurrentTemplateSlot, elem.inventoryItem, elem.MyNumber); //удаляем предмет из json-a танка
            }
            //Destroy(elem.gameObject); //не забыть вынести в специальный асситент для уничтожения реальных объектов пушек
        }

        CorpusElem.ClearSlot(WithReturn); //как теперь вернуть назад платформу в инвентарь?
        _allChildList.Clear();
    }*/

    /// <summary>
    /// Смысл в том, чтобы переписвоить картинку слоту танка на только что сделанный скриншот
    /// и обновить список теплейтов снизу, чтобы этот скриншот установился.
    /// по идее здесь можно бы и перегрузать весь список теплейтов, а подписаться ещё на один делегат 
    /// который обновит картинку у конкретного темплейта
    /// </summary>
    private void UpdateTemplateScreenImage()
    {
        Debug.Log("ActualTemplateSlotNumber = " + ActualTemplateSlotNumber);
        if (ActualTemplateSlotNumber < 0 || ActualTemplateSlotNumber >= TemplateList.Count)
            return;

        //поставить только что полученный скрин танку
        var slot = TemplateList[ActualTemplateSlotNumber];

        if (slot != null)
        {
            slot.ReInit();
            //LoadAllPlayerShip(); //тут это как будто лишнее
        }
    }

    /// <summary>
    /// Вывести характеристики танка при установки или удалении предмета его экипировки
    /// </summary>
    private void UpdateTankCharacteristic()
    {
        //var TankPref = InventoryManager.Instance.GetInventoryItem(CurrentTemplateSlot.id);
        //TankCharacteristicViewer.DrawTankCharacteristic(RealII, TankPref); //пересчитываем характеристики
    }

    /// <summary>
    /// Сохранить текущий шаблон со всеми изменениями
    /// </summary>
    private void SaveCurrentTemplate(InventoryItem itemToAdd)
    {
        if (_currentTemplateSlot == null) //по идее такого быть не может, потому что слот всегда выбран
        {
            Debug.LogError("ConstructorController.Instance.CurCreatingShip == null");
            return;
        }

        InventoryManager.Instance.SavePlayerItems();
    }

    private void RemoveItemFromTamplateBySlotNumber(InventoryItem currentTemplateSlot, EquipmentController equipmentController, int equipmentSlotNumber)
    {
        //удаляем предмет из json-a танка, если оно там есть конечно, перед тем как поставить что-то новое
        InventoryManager.Instance.RemoveItemFromShip(currentTemplateSlot, equipmentSlotNumber, save: false);

        //пропускаем очистку equipmentController при удалении корпуса
        //equipmentController.slots[equipmentSlotNumber].inventoryItem = null; //??
        //equipmentController.slots[equipmentSlotNumber] = null; //по идее должно занулить именно ту пушку, какую надо
    }

    private void RemoveExcessWeaponsForNewCorpus(InventoryItem currentTemplateSlot, MountController mountController, int equipmentSlotNumber)
    {
        //пушки, которые не поместились должны слететь
        if (equipmentSlotNumber == -1)
        {
            InventoryInfoItem tmpII = InventoryManager.Instance.GetInventoryItem(currentTemplateSlot.id);

            for (int i = 0; i < tmpII.containsItems.Count; i++)
            {
                bool hasInNewCorpus = false;

                foreach (var point in mountController.points)
                {
                    if (point.Number == tmpII.containsItems[i].ConstructorSlot || tmpII.containsItems[i].ConstructorSlot == -1)
                    {
                        hasInNewCorpus = true;
                    }
                }

                //если пушка не влезла - убираем её из темплейта (НО БЕЗ СОХРАНЕНИЯ!)
                if (!hasInNewCorpus)
                {
                    tmpII.containsItems.RemoveAt(i);
                }
            }
        }
    }

    //пересоздаём визуал пушек для тех пушек, которые остались в слотах после смены корпуса
    private void ReInitWeaponForNewCorpus(int currentTemplateSlotNumber)
    {
        _shipSlotPm.CurrentPreviewPoint.ReInitModelInCurrentSlot();
    }


    public void CorpusCharacteristicUpdate(int templateNumber)
    {
        _constructorCenterPresenter.CorpusCharacteristicUpdate(_shipSlotPm.GetRealIIBySlotNumber(templateNumber));
    }

    #region Buttons Reactions

    private void OnSelectElement(int elemNumber)
    {
        ShopItem shopItem = _gameDesignConfig.GetShopItemByNumber(elemNumber);

        //конструируем превьюшку 3D-модель того что поставили и записывает данные в класс, но не сохраняем
        SetItemToTemplate(shopItem);
    }

    private void OnMountElement(int elemNumber)
    {
        ShopItem shopItem = _gameDesignConfig.GetShopItemByNumber(elemNumber);

        SaveCurrentTemplate(shopItem.Item); //устанавливаем выбранный предмет
    }

    #endregion Buttons Reactions
}
