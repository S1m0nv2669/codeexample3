﻿using I2.Loc;
using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static ThineGames.Battle.RespawnManager;
using static ThineGames.Logic.RewardManager;

namespace ThineGames.Data
{
    public class GameDesignConfig : MonoBehaviour
    {
        [TabGroup("Shop")] [TableList(ShowIndexLabels = true)] public List<ShopItem> ShopItemList;
        [TabGroup("Shop")] [TableList(ShowIndexLabels = true)]

        [SerializeField] private List<ExpReward> _itemExpRewards;
        public GameObject GunElementPrebaf; //префаб панельки на которую будет выводиться пушка или основа

        [TabGroup("Shop")] public Sprite ClosedItemIcon; 

        [TabGroup ("Leagues")] public List<League> LeagueList; //повышение ранга и смена лиг
        [TabGroup ("Leagues")] public Sprite ClosedLeagueRewardIcon;

        [TabGroup("Chests")] public Chest[] ChestTypePrefList; //Здесь лежат списки на префабы контейнеров
        [TabGroup("Chests")] public int[] FirstChestTimerList; //время открытия первых сундуков

        [TabGroup("Levels")] [ListDrawerSettings(ShowIndexLabels = true)] [SerializeField] private LevelInfo[] _levels; //повышение уровня игрока
        [TabGroup("Levels")] [SerializeField] private int _winExpReward;
        [TabGroup("Levels")] [SerializeField] private int _looseExpReward;

        [TabGroup("Other")]

        [TabGroup("Tutorial")] public bool IsSendAnalitics;
        [TabGroup("Tutorial")] public string TutorialLocalizationPageName = "tutorial2/";
        [TabGroup("Tutorial")] public AudioClip ShowMessageClip; //проигрывается каждый раз при появлении нового сообщения Туториала!
        [TabGroup("Tutorial")] public TutorialStepsGroup[] TutorialStepsGroupList;
        [TabGroup("Tutorial")] public GameObject[] BotPrefabList;

        #region RewardManager

        [TabGroup("RewardManager")] [SerializeField] private RewardType[] _rewards;        //список того, за какие действия игрока ему начисляются в бою очки
        [TabGroup("RewardManager")] [SerializeField] private float _lvlMult = 0.1f;        //награда повышается с уровнем
        [TabGroup("RewardManager")] [SerializeField] private float _softToExpMult = 0.1f;  //Экспириент расчитывается из софта
        [TabGroup("RewardManager")] [SerializeField] private float _winMult = 1.5f;
        [TabGroup("RewardManager")] [SerializeField] private float _adsMult = 2f;          //умножить награду за просмотр рекламы
        [TabGroup("RewardManager")] [SerializeField] private float _decreaseStepCoef;      // на сколько уменьшается награда за каждое повторение шага
        [TabGroup("RewardManager")] [SerializeField] private int _hardCheckLimit = 100;    // сколько очков надо набрать, чтобы добавился хард

        [TabGroup("Battle")] public float AutoRespawnTime;
        [TabGroup("Battle")] public BotUnit[] BattleBotPrefabList;
        [TabGroup("Battle")] public const int MAX_PLAYERS = 6; //плохо, что это задаётся тут, а не в параметрах карты
        [TabGroup("Battle")] public int RespawnDelaySecond = 5;
        [TabGroup("Battle")] public BaseNetworkAvatar PhotonViewPrefab; //префаб игрока
        [TabGroup("Battle")] public GameObject BotPrefab; //for test f4 bot
        [TabGroup("Battle")] public GameObject ZombieBotPrefab; //f5 bot
        [TabGroup("Battle")] public GameObject VideoBotPrefab = null;

        public ShopItem[] GetShopItemListByLeaguesNumber(int leagueNumber)
        {
            return ShopItemList.Where(p => p.NeedLeagueNumber == leagueNumber).ToArray();
        }

        public int GetExpForRareByLevel(int level, ShopItem.RareType rare) 
        {
            return _itemExpRewards.FirstOrDefault(p=>p.Rare == rare).RewardByLevel[level];
        }

        //сюда может быть передана любая валюта, но рарность есть только у вещей в ShopItemList
        //для остальных устанавливаем дефолтную рарность == common
        public ShopItem.RareType GetRareByCurrencyType(CurrencyType currencyType)
        {
            var cart = ShopItemList.FirstOrDefault(p => p.CartType == currencyType);

            if (cart != null)
            {
                return cart.ElemRare;
            }

            return ShopItem.RareType.common;
        }

        public string GetItemIdByCartType(CurrencyType currencyType)
        {
            ShopItem shopItem = ShopItemList.FirstOrDefault(p => p.CartType == currencyType);

            if (shopItem != null)
            {
                return shopItem.Item.id;
            }

            return "";
        }

        public ShopItem GetShopItemByNumber(int shopItemNumber)
        {
            return ShopItemList[shopItemNumber];
        }

        public int GetExtMatchReward(bool isWin) 
        {
            return isWin ? _winExpReward : _looseExpReward;
        } 

        public RewardType[] GetRewardTypes()
        {
            return _rewards;
        }

        #endregion RewardManager

        public LevelInfo[] GetLevelsInfoList()
        {
            return _levels;
        }

        [Serializable]
        public struct ExpReward 
        {
            public ShopItem.RareType Rare;
            public int[] RewardByLevel; 
        }

        [Serializable]
        public class League
        {
            [System.Serializable]
            public struct CamoPair
            {
                public InventoryItem Ship;
                public CamoInventoryItem Camo;
            }

            public Sprite LeagueIcon;
            public string LeagueName;
            public int LeagueNeedElo;
            public int ClosedItemCount;

            //public List<int> DetailItemList; //номера соотвествуют ShopConfig

            public CamoPair[] CamoRewardList; //камуфляжи, которые выдаются за ап ранга

            public string RankName
            {
                get { return LocalizationManager.GetTranslation("ranks/" + LeagueName); }
            }
        }
    }
}