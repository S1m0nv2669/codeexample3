﻿using ThineGames.Data;
using ThineGames.Logic;
using ThineGames.Tutorial;
using ThineGames.UI;
using UnityEngine;

/// <summary>
/// Основная точка входа для логики ангара.
/// </summary>
public class HangarController : SingletonObject <HangarController>
{
    [SerializeField] private GunsTechCenterView _gunsTechCenterView;
    [SerializeField] private ConstructorCenterView _constructorController;
    [SerializeField] private ParentConstructorSlotView _parentConstructorSlotView;
    [SerializeField] private UpgradeWindowView _upgradeWindowView;

    [SerializeField] private ShipSlotViewer _shipSlotViewer;
    [SerializeField] private CloseUpModeController _closeUpModeController;
    [SerializeField] private ShipScreener _shipScreener;
    [SerializeField] private Matchmaking _matchmaking;

    [SerializeField] private UIscr _ui;

    private GameDesignConfig _gameDesignConfig; 

    private GunsTechCenterPresenter _gunsTechCenterPresenter;
    private GunTechCenterLogic _gunTechCenterLogic;

    private ConstructorLogic _constructorLogic;

    private ShipSlotPm _shipSlotPm;
    private LeaguePm _leaguePm;
    private LevelUpPm _levelUpPm;
    private MetaGameEntity _metaGameEntity;
    private ChestSystemLogic _chestSystemLogic;
    private HangarTutorialPm _hangarTutorialPm;
    private WelcomPm _welcomPm;
    private TutorialMessagePm _tutorialMessagePm; //такой существует как для ангара, так и для боевой сцены

    private ReactiveTrigger<int> _onClickSelectElement = new ReactiveTrigger<int>();
    private ReactiveTrigger<int> _onClickMount         = new ReactiveTrigger<int>();
    private ReactiveTrigger<int> _onClickUpgrade       = new ReactiveTrigger<int>();

    private ReactiveTrigger<ShopItem, bool> _onClickLearnUpgrade =  new ReactiveTrigger <ShopItem, bool>();

    private ReactiveTrigger _onClickHideCloseUp = new ReactiveTrigger(); //закрытие окна конструирования танков на кнопку крестик
    private ReactiveTrigger _onClickOpenLeagueWindow = new ReactiveTrigger();

    private ReactiveTrigger <Chest> _onOpenChest             = new ReactiveTrigger<Chest>(); //триггер открытия сундука купленного в магазине
    private ReactiveTrigger <string> _onClickTutorialButton  = new ReactiveTrigger<string>();
    private ReactiveTrigger <string> _tryDoTutorialAction    = new ReactiveTrigger<string>();

    private ReactiveTrigger <int, int> _showTutorialMessage  = new ReactiveTrigger<int, int>();
    private ReactiveTrigger _hideTutorialMessage             = new ReactiveTrigger();


    protected override void Awake()
    {
        base.Awake();

        _gameDesignConfig = Root.Instance.GameDesignConfig;

        _gunsTechCenterPresenter = new GunsTechCenterPresenter(_gunsTechCenterView, _onClickSelectElement, _onClickMount, _onClickUpgrade);

        _ui.SetCtx(_onClickLearnUpgrade, _onClickHideCloseUp, _onClickOpenLeagueWindow, _onOpenChest, _onClickTutorialButton, _gameDesignConfig, _tryDoTutorialAction);

        _gunTechCenterLogic = new GunTechCenterLogic(_gunsTechCenterPresenter,
                                                     _onClickLearnUpgrade, 
                                                     _onClickMount, 
                                                     _onClickUpgrade, 
                                                     _ui,
                                                     _gameDesignConfig, 
                                                     _tryDoTutorialAction);

        _shipSlotPm = new ShipSlotPm(_shipSlotViewer);

        _closeUpModeController.SetCtx(_shipSlotPm, _gunTechCenterLogic, _onClickHideCloseUp, _ui);
        _shipScreener.SetCtx(_shipSlotPm);
        _matchmaking.SetCtx(_shipScreener);

        ConstructorSlotPresenter constructorSlotPresenter = new ConstructorSlotPresenter(_parentConstructorSlotView, _gunsTechCenterView);

        _constructorLogic = new ConstructorLogic(_constructorController,
                                                 constructorSlotPresenter,
                                                 _shipSlotPm, 
                                                 _shipScreener, 
                                                 _onClickMount, 
                                                 _onClickSelectElement,
                                                 _gameDesignConfig);

        _leaguePm = new LeaguePm(_ui, _gameDesignConfig, _onClickOpenLeagueWindow);

        _levelUpPm = new LevelUpPm(_gameDesignConfig.GetLevelsInfoList(), _ui);

        _chestSystemLogic = new ChestSystemLogic(_gameDesignConfig.ChestTypePrefList,
                                                 _ui.ChestDeliveryWindowViewer,
                                                 _ui.ChestOpenWindowViewer,
                                                 _ui.ChestPanelViewer,
                                                 _onOpenChest,
                                                 _leaguePm,
                                                 _ui,
                                                 _gameDesignConfig,
                                                 _tryDoTutorialAction);

        _hangarTutorialPm = new HangarTutorialPm(_ui.HangarTutorialViewer,
                                                 _onClickTutorialButton,
                                                 _tryDoTutorialAction,
                                                 _showTutorialMessage,
                                                 _hideTutorialMessage);

        _tutorialMessagePm = new TutorialMessagePm(_showTutorialMessage,
                                                 _hideTutorialMessage,
                                                 _gameDesignConfig,
                                                 _ui.TutorialMessageViewer,
                                                 _messageWasShown);

        //должна отрабатывать самой последней, так как дёргает другие синглтоны
        _metaGameEntity = new MetaGameEntity(_ui,
                                             _leaguePm,
                                             _levelUpPm,
                                             _gameDesignConfig,
                                             _ui.MatchResultView, 
                                             Root.Instance._gameEventManager,
                                             RootRestart.Instance._metaGameData,
                                             _chestSystemLogic,
                                             _hangarTutorialPm,
                                             _tryDoTutorialAction);

        DevDebug.Instance.SetCtx(_metaGameEntity);

        //запускает туториал
        _welcomPm = new WelcomPm(_ui.WelcomeScreenViewer);
    }

    public void Start()
    {
        _metaGameEntity.OpenWindowAfterLoadHangar();
    }

    public static T CreateGameObjectAndGetScript <T>(GameObject prefab)
    {
        GameObject gameObject = Instantiate(prefab);
        T tmpScript = gameObject.GetComponent<T>();

        return tmpScript;
    }

    public GunTechCenterLogic GetGunTechCenterLogic()
    {
        return _gunTechCenterLogic;
    }

    public ConstructorLogic GetConstructorLogic()
    {
        return _constructorLogic;
    }
}
